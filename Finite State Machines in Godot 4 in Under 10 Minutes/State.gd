extends Node
class_name State


@export var enemy: CharacterBody2D # The enemy that this state controls
@export var move_speed : float # The enemy's movement speed
var player: CharacterBody2D # The player character

signal Transitioned # Emitted when the enemy transitions to a new state

# Called when the enemy enters this state
func enter():
	pass

# Called when the enemy exits this state
func exit():
	pass

# Called every physics frame while the enemy is in this state
func state_process(_delta: float):
	pass

# Called every frame while the enemy is in this state
func state_physics_process(_delta: float):
	pass

extends State
class_name EnemyFollow

func enter():
	# Find the player character
	player = get_tree().get_first_node_in_group("Player")

func state_physics_process(delta: float):
	# Get the direction vector from the enemy to the player
	var direction = player.global_position - enemy.global_position
	
	# If the player is further than 25 pixels away, move towards them
	if direction.length() > 25:
		enemy.velocity = direction.normalized() * move_speed
	# Otherwise, stop moving
	else:
		enemy.velocity = Vector2()
	
	# If the player is further than 50 pixels away, transition to the idle state
	if direction.length() > 50:
		Transitioned.emit(self, "idle")

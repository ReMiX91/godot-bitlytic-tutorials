extends Node

@export var initial_state : State # Initial state of the state machine
var current_state : State # Current state of the state machine
var states : Dictionary = {} # dictionary of all the states in the state machine

func _ready():
	# Iterate over all the child nodes of the state machine
	for child in get_children():
		# If the child node is a State, add it to the dictionary of states
		if child is State:
			states[child.name.to_lower()] = child
			# Connect the Transitioned signal of the child node to the on_child_transition function of the state machine
			child.Transitioned.connect(on_child_transition)
	
	# If the initial state is set, enter it and set it as the current state
	if initial_state:
		initial_state.enter()
		current_state = initial_state

func _process(delta):
	# Apply state process
	if current_state:
		current_state.state_process(delta)

func _physics_process(delta):
	# Apply physics process
	if current_state:
		current_state.state_physics_process(delta)

func on_child_transition(state, new_state_name):
	# If the state that emitted the signal is not the current state, ignore it
	if state != current_state:
		return

	# Get the new state from the dictionary of states.
	var new_state = states.get(new_state_name.to_lower())
	
	# If the new state doesn't exist, ignore it
	if !new_state:
		return
	
	# Exit the current state
	if current_state:
		current_state.exit()
	
	# Enter the new state
	new_state.enter()
	
	# Set the current state to the new state.
	current_state = new_state

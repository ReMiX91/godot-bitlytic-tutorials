extends State
class_name EnemyIdle

var move_direction : Vector2 # Direction in which the enemy is wandering
var wander_time : float # Amount of time remaining before the enemy changes its wandering direction

# Randomizes the enemy's wandering direction
func randomize_wander():
	move_direction = Vector2(randf_range(-1, 1), randf_range(-1, 1)).normalized()
	wander_time = randf_range(1, 3)

func enter():
	# Find the player character
	player = get_tree().get_first_node_in_group("Player")
	randomize_wander()

func state_process(delta: float):
	# If the enemy's wandering time has expired, randomize a new wandering direction
	if wander_time > 0:
		wander_time -= delta
	else:
		randomize_wander()

func state_physics_process(delta: float):
	# Move in the wandering direction
	if enemy:
		enemy.velocity = move_direction * move_speed
	
	# Get the direction vector from the enemy to the player.
	var direction = player.global_position - enemy.global_position
	
	# If the player is closer than 30 pixels away, transition to the follow state
	if direction.length() < 30:
		Transitioned.emit(self, "follow")
